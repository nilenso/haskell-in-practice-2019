# Common types
- String - This is just really [Char]
```
Prelude> :t "Hello world"
"Hello world" :: [Char]
```
- Bool
- List
- Maybe
- Tuples
```
> :t (,)
(,) :: a -> b -> (a, b)

> :t (,,)
(,,) :: a -> b -> c -> (a, b, c)

> :t (,,,)
(,,,) :: a -> b -> c -> d -> (a, b, c, d)

```
- Unit - This type has only one member, which is ()
```
> :t ()
() :: ()
```
