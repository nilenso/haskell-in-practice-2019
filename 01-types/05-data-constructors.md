# Data constructors
- Data constructors are used to create higher order types.
- `:info` can be used to find the data constructors of types
```
> :info Maybe
data Maybe a = Nothing | Just a 	-- Defined in ‘GHC.Maybe’
{- ... -}

> :info Either
data Either a b = Left a | Right b 	-- Defined in ‘Data.Either’
{- ... -}
```
- In fact, data constructors are simple functions
```
> :t Just
Just :: a -> Maybe a

> :type Nothing
Nothing :: Maybe a

> :type Left
Left :: a -> Either a b

> :type Right
Right :: b -> Either a b
```
