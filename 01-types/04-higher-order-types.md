# Higher order types
- These are types that take a type and return a new type.
```
-- Here [] is a higher order type takes a type Char and returns a new type [Char]
-- [a] represents a list of Char. Infact [] is a syntactic sugar over List
> :t ['a', 'b', 'c', 'd', 'e']
[1,2,3,4,5] :: [Char]

-- Here Maybe is a type that takes a type Char and returns new type Maybe Char
-- Maybe Char represents a value that could be a Char or Nothing
> :t Just 'a'
Just 'a' :: Maybe Char

-- Higher order types can also take multiple arguments
-- Here Either is a type that takes `Char` and `Int` as a arguements and returns an new type.
-- Either Char Int represents a value that can be either a Char or an Int
Prelude> :t (Left 'a' :: Either Char Int)
(Left 'a' :: Either Char Int) :: Either Char Int

-- Haskell tuples are types that can many different types and return a single new type
> :t (1,2,3,4)
(1,2,3,4) :: (Num a, Num b, Num c, Num d) => (a, b, c, d)
```
