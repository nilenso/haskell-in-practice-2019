# Types
- [Haskell Wiki: Type](https://wiki.haskell.org/Type)
- Types describe the data that your program will work with
- All expressions in haskell are typed
  ```
  > :t "Hello World!"
  "Hello World!" :: [Char]

  > :t (1 + 1)
  (1 + 1) :: Num a => a

  > :t (+)
  (+) :: Num a => a -> a -> a

  > :t Data.Char.intToDigit
  Data.Char.intToDigit :: Int -> Char
  ```
- A type of a value means that it shares certain behavior with other values of the same type
- However, the type of a value only describes a value, it does not describe the behavior
- Types in haskell have 3 properties.
  - Values are strongly typed
  - The types are static.
    - The compiler knows about the types of values at compile time and type checks the program.
  - The compiler can infer types for most values.
- You have to pass the right type of values to functions
  ```
  > Data.Char.intToDigit 1
  '1'
  > Data.Char.intToDigit '1'
  
  <interactive>:7:22: error:
      • Couldn't match expected type ‘Int’ with actual type ‘Char’
      • In the first argument of ‘GHC.Show.intToDigit’, namely ‘'1'’
        In the expression: GHC.Show.intToDigit '1'
        In an equation for ‘it’: it = GHC.Show.intToDigit '1'
  ```
- However, haskell is has excellent support for type inference
  - Writing the type of a value is not necessary
    - The compiler sometimes needs hints to figure out the types
  - It is best practice to write types for function definitions, as the types also serve as documentation

