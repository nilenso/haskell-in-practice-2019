# Defining your own data types
## `data` keyword
- The primary way of creating a new data type is using the `data` keyword.
```
data BookType = SciFi | Political | Textbook | Publication | Classic | OtherType String

data Book author = Unknown
                 | BookName String
                 | BookWithAuthor String author
                 | BookWithType String author BookType

animalFarm = BookName "Animal Farm"
book1984 = BookWithType "1984" "George Orwell" (OtherType "distopia")
```
- The first capitalized word of each declartion seperated by `|` is a **data constructor** for it's type
- A type can take no arguments like `BookType`, or take one or more arguments like `Book`, `Maybe` and `Either`
- The words following the data constructor are the types of arguments of the type constructor.
- This syntax does not allow us to name what each type represents. We can use the record syntax for this.
```
data Author = Author String String Int

data AuthorRecord = AuthorRecord
  { name :: String
  , country_of_origin :: String
  , age :: Int
  }

authorRecord = AuthorRecord 
  { name = "Haruki Murakami"
  , country_of_origin = "Japan"
  , age = "70"
  }

```
- The record syntax creates functions with the same name for each key
```
> :t name
name :: AuthorRecord -> String
> :t country_of_origin
country_of_origin :: AuthorRecord -> String
> :t age
age :: AuthorRecord -> Int
```

## `type` keyword
- The `type` key word is used to create an alias for an existing type.
```
type Country = String
type Age = Int
type BookAuthorRecord = Book AuthorRecord

type String = [Char]
type ResultOrErrorCode a = Either Int a
type ResultOrErrorCode = Either Int

type Handler req res = Request req -> Response res
```
- The `type` keyword does not create a new type. It just creates an alias for the type. 
- The type signatures of functions can also be type aliased
- This is helpful when we want to name complex types.

## `newtype` keyword
- `newtype` keyword creates a new data type just like the data keyword.
- Types created with `newtype` keyword can have only one data constructor which takes only one argument.
- The declaration can however have as many type arguments as necessary
```
newtype Name = Name string
newtype Country = Country { unCountry :: String }
newtype State s a = State { runState :: s -> (a, s) }
```
- The restriction to one constructor with one field means that the new type and the type of the field are in direct correspondence. (from https://wiki.haskell.org/Newtype)
- This allows the complier to differentiate between the two types at compile time, but treat them as the same during runtime
- This gives us additional type safety at compile time, which avoid any extra overhead at runtime.
# Introduction to type classes
# Type inference in haskell
# Exercises in defining types
