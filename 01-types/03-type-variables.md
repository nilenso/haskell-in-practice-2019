# Type variables
- Int the previous examples, we have a type `a` in the type `(+)` and `take`
- Here `a` is used as **type variable**. This means, any the function can accept any type as it's arguments, given the type meets a few constraints.
- A type definition can have one or more type variables.
```
> :t map
map :: (a -> b) -> [a] -> [b]

> :t maybe
maybe :: b -> (a -> b) -> Maybe a -> b

> :t either
either :: (a -> c) -> (b -> c) -> Either a b -> c
```
- In the case of `(+)`, the function can be used used to add any type that meets a constraint called `Num`
- To see the types that meet a constraint, you can use `:info <ConstraintName>` in ghci
```
> :info Num
class Num a where

{- ... -}

instance Num Word -- Defined in ‘GHC.Num’
instance Num Integer -- Defined in ‘GHC.Num’
instance Num Int -- Defined in ‘GHC.Num’
instance Num Float -- Defined in ‘GHC.Float’
instance Num Double -- Defined in ‘GHC.Float’
```
- `Word`, `Integer`, `Int`, `Float`, and `Double` meet the `Num` constraint and `(+)` can be used to add them.
- The constraints are called **Typeclases**, and they'll be introduced in a later section
- To add a constraint to a type definition, `=>` is used. 
```
-- Here a should meet the constraint Num
(+) :: Num a => a -> a -> a

-- Here a should meet the constraint Num
(==) :: Eq a => a -> a -> Bool

-- Multiple contraints can also be added to a type variable
f :: (Eq a, Show a) => a -> b

-- Type constraints can also be added to the return value of a function
f :: (Eq a, Num b) => a -> b
```
