# Types for functions
- The type of a function describes the type it's arguments and the type of it's return value
- All functions in haskell return only a single value.
- The last type in a functions definition, is the type of it's return value.
- Type of each argument is separated by `->`. The type of the final argument is followed by 
- `intToDigit` has a type of `Int -> Char`. Here the function takes a single argument of type `Int` and returns a value of type `Char`
  - In java, `intToDigit` could be declared as `char intToDigit(int i)`
- `take` is a function that takes an `Int` (n), and a list of elements, and returns the first n elements. 
  ```
  > take 2 [1, 2, 3, 4, 5]
  [1,2]
  > :t take
  take :: Int -> [a] -> [a] 
  ```
- `take` takes two arguments
  - the first argument is an `Int`
  - the second argument is a List of element `[a]`
  - the return value also is a List of elements `[a]`
  - type of elements in the argument list, is the same as the elements in the returned list
  - in java `take` could be declared as `List<a> take(int n, List<a> list)`
    > Note: Comparison with java is only helpful for initial understanding.
