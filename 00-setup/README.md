- What is haskell?
  - Haskell is a statically typed, purely functional, lazily evaluated programming language.
  - All Haskell data structures are immutable by default
  - All haskell functions are referentially transparent
    - calling a function with the same arguments will always return the same value
    - all function are stateless
  - https://www.haskell.org
- Brief history
  - Developed as a committee language by academicians.
  - Many silo-ed programming with various experimental features were being developed by various academic groups
  - Most languages could not gather critical mass in terms of it's users.
  - Initial meeting in 1987 to garner consent for consolidating efforts to designing a new common functional programming language.
  - http://haskell.cs.yale.edu/wp-content/uploads/2011/02/history.pdf
- Why is it increasing in relevance now?
  - Many companies have historically used and continue to use haskell.
  - Prominent companies include Google, Microsoft, Facebook, Intel, NVIDIA, NYT.
  - https://wiki.haskell.org/Haskell_in_industry
  - Many other companies are adopting function programming to create solutions.
  - Functional programming concepts like immutability, statelessness, referential transparency, make it easier to reason about an application
- installing haskell
  - https://www.haskell.org/platform/
  - Install haskell through stack
    - https://docs.haskellstack.org/en/stable/README/
    ```
    $ stack setup
    ```
- running things in the repl 
  - Start the repl
  ```
  $ ghci
  GHCi, version 8.6.4: http://www.haskell.org/ghc/  :? for help
  Prelude>
  ```
  - The repl can used used to run valid haskell code
  ```
  Prelude> 1 + 1
  2
  Prelude> a = [1, 2, 3, 4]
  Prelude> print a
  [1,2,3,4]
  Prelude> :t take
  take :: Int -> [a] -> [a]
  Prelude> take 2 a
  [1,2]
  Prelude>
  ```
- executing scripts
  - Create a new file called FirstScript.hs
  ```
  main :: IO ()
  main = print "Hello World!"
  ```
  - To run the script
  ```
  $ stack ghc -- FirstScript.hs
  ```
  - To create a executable from the script, add `#!/usr/bin/env stack` to the top of the file
  ```
  #!/usr/bin/env stack


  main :: IO ()
  main = print "Hello World!"
  ```
  - Now you can run the script using the following commands
  ```
  stack FirstScript.hs
  # or
  chmod +x ./FirstScript.hs
  ./FirstScript.hs
  ```
- hoogle, hackage, and stackage
  - hoogle - https://hoogle.haskell.org/
    - Hoogle is a search engine for haskell functions
    - You can search for functions using names or types
  - hackage - https://hackage.haskell.org/
    - Hackage is a repository for haskell packages
  - stackage - https://www.stackage.org/
    - Stackage is a curated repository of haskell packages
    - This is used by stack for building packages.
    - stack can fallback to hackage if a package is not available on stackage.
